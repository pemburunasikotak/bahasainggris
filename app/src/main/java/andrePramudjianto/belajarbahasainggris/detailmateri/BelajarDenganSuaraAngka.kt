package andrePramudjianto.belajarbahasainggris.detailmateri

import andrePramudjianto.belajarbahasainggris.R
import andrePramudjianto.belajarbahasainggris.materividio.MateriBahasaInggris
import andrePramudjianto.belajarbahasainggris.materividio.Model
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.GridLayout
import android.widget.HorizontalScrollView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_belajar_dengan_suara.*

class BelajarDenganSuaraAngka : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_belajar_dengan_suara)

        val bundle = intent.extras

        val data = bundle?.getString("nama")
        btn_learnwithvidio2.setOnClickListener(){
            val intent = Intent (this, MateriBahasaInggris::class.java)
            startActivity(intent)
        }

        if (data.equals("Angka")) {
            val model = ArrayList<model>()
            model.add(model("1","angka 1", R.drawable.angka1,R.raw.truk))
            model.add(model("2","angka 2", R.drawable.angka2,R.raw.truk))
            model.add(model("3","angka 3", R.drawable.angka3,R.raw.truk))
            model.add(model("4","angka 4", R.drawable.angka4,R.raw.truk))
            model.add(model("5","angka 5", R.drawable.angka5,R.raw.truk))
            model.add(model("6","angka 6", R.drawable.angka6,R.raw.truk))
            model.add(model("7","angka 7", R.drawable.angka7,R.raw.truk))
            model.add(model("8","angka 8", R.drawable.angka8,R.raw.truk))
            model.add(model("9","angka 9", R.drawable.angka9,R.raw.truk))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }
        else if (data == "Benda"){
            val modelBenda = ArrayList<model>()
            modelBenda.add(model("1","BackPack", R.drawable.benda6,R.raw.backpack))
            modelBenda.add(model("2","BlackBoard", R.drawable.benda5,R.raw.blackboard))
            modelBenda.add(model("3","Book", R.drawable.benda4,R.raw.book))
            modelBenda.add(model("4","Chair", R.drawable.benda1,R.raw.chair))
            modelBenda.add(model("5","Door", R.drawable.benda7,R.raw.door))
            modelBenda.add(model("6","Pencil", R.drawable.benda3,R.raw.pencil))
            modelBenda.add(model("7","Shoe", R.drawable.benda9,R.raw.shoe))
            modelBenda.add(model("8","Table", R.drawable.benda2,R.raw.table))
            modelBenda.add(model("9","Window", R.drawable.benda8,R.raw.window))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, modelBenda, { model -> personItemClicked(modelBenda as model)  })
        }
        else if (data == "Buah"){
            val model = ArrayList<model>()
            model.add(model("1","Appel", R.drawable.buah2,R.raw.apple))
            model.add(model("2","Banana", R.drawable.buah5,R.raw.banana))
            model.add(model("3","Durian", R.drawable.buah7,R.raw.durian_2))
            model.add(model("4","Grape", R.drawable.buah1,R.raw.grape))
            model.add(model("5","Mangosteen", R.drawable.buah8,R.raw.mangosteen))
            model.add(model("6","Orange", R.drawable.buah3,R.raw.orange))
            model.add(model("7","Pinappple", R.drawable.buah9,R.raw.pineapple))
            model.add(model("8","Strawberry", R.drawable.buah6,R.raw.strawberry))
            model.add(model("9","WaterMelon", R.drawable.buah4,R.raw.watermelon))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }
        else if (data == "Hari"){
            val model = ArrayList<model>()
            model.add(model("1","Senin", R.drawable.hari,R.raw.apple))
            model.add(model("2","Selasa", R.drawable.buah,R.raw.apple))
            model.add(model("3","Rabo", R.drawable.hari,R.raw.apple))
            model.add(model("4","Kamis", R.drawable.buah,R.raw.apple))
            model.add(model("5","Jum'at", R.drawable.hari,R.raw.apple))
            model.add(model("6","Saptu", R.drawable.buah,R.raw.apple))
            model.add(model("7","Minggu", R.drawable.hari,R.raw.apple))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }
        else if (data == "Hewan"){
            val model = ArrayList<model>()
            model.add(model("1","Bird", R.drawable.hewan8,R.raw.bird))
            model.add(model("2","Elephant", R.drawable.hewan1,R.raw.elephant))
            model.add(model("3","giraffe", R.drawable.hewan3,R.raw.giraffe))
            model.add(model("4","Horse", R.drawable.hewan4,R.raw.horse))
            model.add(model("5","Lion", R.drawable.hewan9,R.raw.lion))
            model.add(model("6","Monkey", R.drawable.hewan5,R.raw.monkey))
            model.add(model("7","Panda", R.drawable.hewan7,R.raw.panda))
            model.add(model("8","Snake", R.drawable.hewan6,R.raw.snake))
            model.add(model("9","Tiger", R.drawable.hewan2,R.raw.tiger))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })

        }
        else if (data == "Transportasi"){
            val model = ArrayList<model>()
            model.add(model("1","Bicyle", R.drawable.transportasi7,R.raw.bicycle))
            model.add(model("2","Bus", R.drawable.transportasi1,R.raw.bus_2))
            model.add(model("3","Car", R.drawable.transportasi8,R.raw.car))
            model.add(model("4","MotoCyle", R.drawable.transportasi6,R.raw.motorcycle))
            model.add(model("5","Pedicab", R.drawable.transportasi5,R.raw.pedicab))
            model.add(model("6","Ship", R.drawable.transportasi3,R.raw.ship))
            model.add(model("7","Train", R.drawable.transportasi4,R.raw.train))
            model.add(model("8","Truck", R.drawable.transportasi9,R.raw.truk))
            model.add(model("9","Truck", R.drawable.transportasi9,R.raw.truk))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }
        else if (data == "AnggotaTubuh"){
            val model = ArrayList<model>()
            model.add(model("1","Ear", R.drawable.organtubuh1,R.raw.ear))
            model.add(model("2","Eyes", R.drawable.organtubuh4,R.raw.eyes))
            model.add(model("3","Foot", R.drawable.organtubuh6,R.raw.foot))
            model.add(model("4","Hand", R.drawable.organtubuh5,R.raw.hand))
            model.add(model("5","Heart", R.drawable.organtubuh8,R.raw.heart))
            model.add(model("6","Intestine", R.drawable.organtubuh7,R.raw.intestine))
            model.add(model("7","Lungs", R.drawable.organtubuh9,R.raw.lungs))
            model.add(model("8","Nose", R.drawable.organtubuh2,R.raw.nose))
            model.add(model("8","Tongue", R.drawable.organtubuh3,R.raw.tongue))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }
        else if (data == "Sayuran"){
            val model = ArrayList<model>()
            model.add(model("1","Cabbage", R.drawable.sayuran6,R.raw.cabbage))
            model.add(model("2","Carrot", R.drawable.sayuran2,R.raw.carrot))
            model.add(model("3","Chili", R.drawable.sayuran8,R.raw.chili))
            model.add(model("4","Corn", R.drawable.sayuran1,R.raw.corn))
            model.add(model("5","Eggplant", R.drawable.sayuran3,R.raw.eggplant))
            model.add(model("6","Garlic", R.drawable.sayuran9,R.raw.garlic))
            model.add(model("7","Mustard", R.drawable.sayuran5,R.raw.mustard))
            model.add(model("8","Potato", R.drawable.sayuran7,R.raw.potato))
            model.add(model("9","Tomato", R.drawable.sayuran4,R.raw.tomato))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }
        else if (data == "Bulan"){
            val model = ArrayList<model>()
            model.add(model("1","Januari", R.drawable.bulan,R.raw.apple))
            model.add(model("2","Februari", R.drawable.anggotatubuh,R.raw.apple))
            model.add(model("3","Maret", R.drawable.transportasi,R.raw.apple))
            model.add(model("4","April", R.drawable.bulan,R.raw.apple))
            model.add(model("1","Mei", R.drawable.bulan,R.raw.apple))
            model.add(model("2","Juni", R.drawable.anggotatubuh,R.raw.apple))
            model.add(model("3","Juli", R.drawable.transportasi,R.raw.apple))
            model.add(model("4","Agustus", R.drawable.bulan,R.raw.apple))
            model.add(model("1","September", R.drawable.bulan,R.raw.apple))
            model.add(model("2","Oktober", R.drawable.anggotatubuh,R.raw.apple))
            model.add(model("3","November", R.drawable.transportasi,R.raw.apple))
            model.add(model("4","Desember", R.drawable.bulan,R.raw.apple))
            rv_materi_suara.layoutManager= GridLayoutManager(this,3,GridLayoutManager.VERTICAL, false)
            rv_materi_suara.adapter= adapter(this, model, { model -> personItemClicked(model as model)  })
        }

    }
    private fun personItemClicked(person: model) {
        Toast.makeText(applicationContext,person.nama, Toast.LENGTH_LONG)
//        val bundel = Bundle()
//        bundel.putString("name", person.name)
//        bundel.putString("doatext", person.doatext)
//        bundel.putString("doaarti", person.doatextarti)
//        bundel.putInt("lagu",person.doasuara)
////        bundel.("goldar",person.doasuara)
//        val intent = Intent(this,DetailDoa::class.java)
//        intent.putExtras(bundel)
//        startActivity(intent)

    }
}