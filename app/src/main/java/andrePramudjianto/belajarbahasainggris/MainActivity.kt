package andrePramudjianto.belajarbahasainggris

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
//import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
//import java.net.URL


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn_login_siswa.setOnClickListener {
            val intent = Intent(this, inputnamasiswa::class.java)
            startActivity(intent)
        }
        btn_login_admin.setOnClickListener {
            val intent = Intent(this, LoginAdmin::class.java)
            startActivity(intent)
        }

    }
}